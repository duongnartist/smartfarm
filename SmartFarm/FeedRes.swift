//
//  FeedRes.swift
//  SmartFarm
//
//  Created by Nguyễn Dương on 8/27/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import Foundation
import EVReflection

class FeedRes: BaseRes {
  var data: [FeedObj] = []
}

class FeedObj: EVObject {

  var id: NSNumber? = 0
  var _id: Int {
    return id?.intValue ?? 0
  }
  
  var time: NSString? = ""
  var _time: String {
    return time as String? ?? ""
  }
  
  var weight: NSNumber?
  var _weight: Int {
    return weight?.intValue ?? 0
  }
  
  var number: NSString?
  var _number: String {
    return number as String? ?? ""
  }
  
}
