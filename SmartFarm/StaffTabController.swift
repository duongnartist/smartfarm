//
//  StaffTabController.swift
//  SmartFarm
//
//  Created by Nguyễn Dương on 9/1/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit
import MFSideMenu

class StaffTabController: UIViewController {

  @IBOutlet weak var bottomTabView: BorderCornerView!

  @IBOutlet weak var tabView0: UIView!
  @IBOutlet weak var tabView1: UIView!
  @IBOutlet weak var tabView2: UIView!
  @IBOutlet weak var tabView3: UIView!

  var tabViews = [UIView]()

  override func viewDidLoad() {
    super.viewDidLoad()
    menuContainerViewController.panMode = MFSideMenuPanModeNone
    NotificationCenter.default.addObserver(self, selector: #selector(showLeftMenu), name: NSNotification.Name("ShowLeftMenu"), object: nil)
    
    tabViews = [tabView0, tabView1, tabView2, tabView3]
    self.displayTab(index: 0)
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }

  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    prepareForEmbedBottomBar(segue: segue)
  }

  func prepareForEmbedBottomBar(segue: UIStoryboardSegue) {
    if segue.identifier == R.segue.staffTabController.embedBottomTab.identifier {
      if let staffBottomController: StaffBottomController = segue.destination as? StaffBottomController {
        staffBottomController.onSelectedTab = { selectedTabIndex in
          self.displayTab(index: selectedTabIndex)
        }
      }
    }
  }

  func displayTab(index: Int) {
    for i in 0 ..< self.tabViews.count {
      self.tabViews[i].isHidden = i != index
    }
  }

  func showLeftMenu() {
    menuContainerViewController.toggleLeftSideMenuCompletion({
      self.menuContainerViewController.viewWillAppear(true)
    })
  }
}
