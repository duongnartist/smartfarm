//
//  FeedCell.swift
//  SmartFarm
//
//  Created by Nguyễn Dương on 8/27/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit

class FeedCell: UITableViewCell {

  @IBOutlet weak var indexLabel: UILabel!
  @IBOutlet weak var timeLabel: UILabel!
  @IBOutlet weak var weightLabel: UILabel!
  @IBOutlet weak var typeLabel: UILabel!

  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }

  func display(feed: FeedObj) {
    indexLabel.text = "# \(feed._id)"
    timeLabel.text = feed._time
    weightLabel.text = feed._weight.toString
    typeLabel.text = feed._number
  }
}
