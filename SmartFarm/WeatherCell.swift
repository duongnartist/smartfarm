//
//  WeatherCell.swift
//  SmartFarm
//
//  Created by Nguyễn Dương on 9/4/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit
import Kingfisher

class WeatherCell: UICollectionViewCell {
  
  @IBOutlet weak var dateLabel: UILabel!
  @IBOutlet weak var iconImageView: UIImageView!
  @IBOutlet weak var tempLabel: UILabel!
  @IBOutlet weak var tempRangeLabel: UILabel!
  
  var weatherObj: WeatherObj!
  
  func display(weatherObj: WeatherObj, isC: Bool) {
    self.weatherObj = weatherObj
    if let url: URL = URL(string: self.weatherObj._hourly._weatherIconUrl) {
      iconImageView.kf.setImage(with: url)
    } else {
      iconImageView.image = nil
    }
    dateLabel.text = self.weatherObj._date
    //°F °C
    let temp = isC ? self.weatherObj._hourly._tempC : self.weatherObj._hourly._tempF
    let unit = isC ? "°C" : "°F"
    tempLabel.text = String(format: "%d %@", temp, unit)
    let minTemp = isC ? self.weatherObj._mintempC : self.weatherObj._mintempF
    let maxTemp = isC ? self.weatherObj._maxtempC : self.weatherObj._maxtempF
    tempRangeLabel.text = String(format: "%d - %d %@", minTemp, maxTemp, unit)
  }
}
