//
//  PillHistoriesController.swift
//  SmartFarm
//
//  Created by Nguyễn Dương on 8/27/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit

class MedicineHistoriesController: UITableViewController {
  
  var lakes = [LakeObj]()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    for i in 0 ... 20 {
      let lake = LakeObj()
      lake.lake_name = "Ao \(i)" as NSString
      lakes.append(lake)
    }
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
  
}

extension MedicineHistoriesController: UICollectionViewDelegate, UICollectionViewDataSource {
  
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return lakes.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.miniLakeCell.identifier, for: indexPath) as! MiniLakeCell
//    if let lake = lakes.get(at: indexPath.row) {
//      cell.display(lake: lake)
//    }
//    return cell
    return UICollectionViewCell()
  }
}
