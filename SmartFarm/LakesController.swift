//
//  LakesViewController.swift
//  SmartFarm
//
//  Created by Nguyễn Dương on 8/17/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit
import EZSwiftExtensions
import EZAlertController
import MFSideMenu

class LakesController: UICollectionViewController {

  var LakeData = [LakeObj]()
  var LakeSize: CGSize!

  var isRequesting = false

  override func viewDidLoad() {
    super.viewDidLoad()
    menuContainerViewController.panMode = MFSideMenuPanModeNone

    let dimens = (ez.screenWidth - 30) / 2
    LakeSize = CGSize(width: dimens, height: dimens)
    
    Request.getFoodType(complete: { res in
      print(res)
    })
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    getLakes()
  }

  func getLakes() {
    guard isRequesting else {
      show(requesting: &isRequesting)
      Request.getAllLake(complete: { res in
        self.hide(requesting: &self.isRequesting)
        if res.isSuccess {
          self.LakeData = res.data
        } else {
          self.LakeData.removeAll()
        }
        self.collectionView?.reloadData()
      })
      return
    }
  }

  @IBAction func menuButtonAction(_ sender: Any) {
    menuContainerViewController.toggleLeftSideMenuCompletion({
      self.menuContainerViewController.viewWillAppear(true)
    })
  }
}

extension LakesController: UICollectionViewDelegateFlowLayout {

  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    return LakeSize
  }

  override func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }

  override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return LakeData.count
  }

  override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.lakeCell.identifier, for: indexPath) as! LakeCell
//    cell.display(Lake: LakeData[indexPath.row])
//    return cell
    return UICollectionViewCell()
  }
}
