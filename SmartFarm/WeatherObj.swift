//
//  WeatherObj.swift
//  SmartFarm
//
//  Created by Nguyễn Dương on 9/1/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import Foundation
import EVReflection

class WeatherRes: EVObject {
  var data: [WeatherDataObj] = []
  var _data: WeatherDataObj {
    if data.isEmpty {
      return WeatherDataObj()
    }
    return data.first!
  }
}

class WeatherDataObj: EVObject {
  var request: [RequestObj] = []
  var weather: [WeatherObj] = []
}

class RequestObj: EVObject {
  var type: NSString?
  var _type: String {
    return type as String? ?? ""
  }
  
  var query: NSString?
  var _query: String {
    return query as String? ?? ""
  }
}

class WeatherObj: EVObject {
  var date: NSString?
  var _date: String {
    return date as String? ?? ""
  }
  
  var astronomy: [AstronomyObj] = []
  var _astronomy: [AstronomyObj] {
    return astronomy
  }
  
  var maxtempC: NSNumber?
  var _maxtempC: Int {
    return maxtempC?.intValue ?? 0
  }
  
  var maxtempF: NSNumber?
  var _maxtempF: Int {
    return maxtempF?.intValue ?? 0
  }
  
  var mintempC: NSNumber?
  var _mintempC: Int {
    return mintempC?.intValue ?? 0
  }
  
  var mintempF: NSNumber?
  var _mintempF: Int {
    return mintempF?.intValue ?? 0
  }
  
  var tides: [Tide] = []
  var _tides: [Tide] {
    return tides
  }
  
  var hourly: [Hourly] = []
  var _hourly: Hourly {
    if hourly.isEmpty {
      return Hourly()
    }
    return hourly.first!
  }
}

class AstronomyObj: EVObject {
  var sunrise: NSString?
  var _sunrise: String {
    return sunrise as String? ?? ""
  }
  
  var sunset: NSString?
  var _sunset: String {
    return sunset as String? ?? ""
  }
  
  var moonrise: NSString?
  var _moonrise: String {
    return moonrise as String? ?? ""
  }
  
  var moonset: NSString?
  var _moonset: String {
    return moonset as String? ?? ""
  }
}

class Tide: EVObject {
  var tide_data: [TideData] = []
}

class TideData: EVObject {
  var tideTime: NSString?
  var _tideTime: String {
    return tideTime as String? ?? ""
  }
  
  var tideHeight_mt: NSNumber?
  var _tideHeight_mt: Double {
    return tideHeight_mt?.doubleValue ?? 0.0
  }
  
  var tideDateTime: NSString?
  var _tideDateTime: String {
    return tideDateTime as String? ?? ""
  }
  
  var tide_type: NSString?
  var _tide_type: String {
    return tide_type as String? ?? ""
  }
}

class Hourly: EVObject {
  var time: NSNumber?
  var _time: Int {
    return time?.intValue ?? 0
  }
  
  var tempC: NSNumber?
  var _tempC: Int {
    return tempC?.intValue ?? 0
  }
  
  var tempF: NSNumber?
  var _tempF: Int {
    return tempF?.intValue ?? 0
  }
  
  var windspeedMiles: NSNumber?
  var _windspeedMiles: Int {
    return windspeedMiles?.intValue ?? 0
  }
  
  var windspeedKmph: NSNumber?
  var _windspeedKmph: Int {
    return windspeedKmph?.intValue ?? 0
  }
  
  var winddirDegree: NSNumber?
  var _winddirDegree: Int {
    return winddirDegree?.intValue ?? 0
  }
  
  var winddir16Point: NSString?
  var _winddir16Point: String {
    return winddir16Point as String? ?? ""
  }
  
  var weatherCode: NSNumber?
  var _weatherCode: Int {
    return weatherCode?.intValue ?? 0
  }
  
  var weatherIconUrl: [WeatherIconUrl] = []
  var _weatherIconUrl: String {
    if weatherIconUrl.isEmpty {
      return ""
    }
    return (weatherIconUrl.first?._value)!
  }
  
  var weatherDesc: [WeatherIconUrl] = []
  var _weatherDesc: String {
    if weatherDesc.isEmpty {
      return ""
    }
    return (weatherDesc.first?._value)!
  }
  
  var lang_vi: [LangVi] = []
  var _lang_vi: String {
    if lang_vi.isEmpty {
      return ""
    }
    return (lang_vi.first?._value)!
  }
  
  var precipMM: NSNumber?
  var _precipMM: Double {
    return precipMM?.doubleValue ?? 0.0
  }
  
  var humidity: NSNumber?
  var _humidity: Int {
    return humidity?.intValue ?? 0
  }
  
  var visibility: NSNumber?
  var _visibility: Int {
    return visibility?.intValue ?? 0
  }
  
  var pressure: NSNumber?
  var _pressure: Int {
    return pressure?.intValue ?? 0
  }
  
  var cloudcover: NSNumber?
  var _cloudcover: Int {
    return cloudcover?.intValue ?? 0
  }
  
  var HeatIndexC: NSNumber?
  var _HeatIndexC: Int {
    return HeatIndexC?.intValue ?? 0
  }
  
  var HeatIndexF: NSNumber?
  var _HeatIndexF: Int {
    return HeatIndexF?.intValue ?? 0
  }
  
  var DewPointC: NSNumber?
  var _DewPointC: Int {
    return DewPointC?.intValue ?? 0
  }
  
  var DewPointF: NSNumber?
  var _DewPointF: Int {
    return DewPointF?.intValue ?? 0
  }
  
  var WindChillC: NSNumber?
  var _WindChillC: Int {
    return WindChillC?.intValue ?? 0
  }
  
  var WindChillF: NSNumber?
  var _WindChillF: Int {
    return WindChillF?.intValue ?? 0
  }
  
  var WindGustMiles: NSNumber?
  var _WindGustMiles: Int {
    return WindGustMiles?.intValue ?? 0
  }
  
  var WindGustKmph: NSNumber?
  var _WindGustKmph: Int {
    return WindGustKmph?.intValue ?? 0
  }
  
  var FeelsLikeC: NSNumber?
  var _FeelsLikeC: Int {
    return FeelsLikeC?.intValue ?? 0
  }
  
  var FeelsLikeF: NSNumber?
  var _FeelsLikeF: Int {
    return FeelsLikeF?.intValue ?? 0
  }
  
  var sigHeight_m: NSNumber?
  var _sigHeight_m: Double {
    return sigHeight_m?.doubleValue ?? 0
  }
  
  var swellHeight_m: NSNumber?
  var _swellHeight_m: Double {
    return swellHeight_m?.doubleValue ?? 0
  }
  
  var swellHeight_ft: NSNumber?
  var _swellHeight_ft: Double {
    return swellHeight_ft?.doubleValue ?? 0
  }
  
  var swellDir: NSNumber?
  var _swellDir: Int {
    return swellDir?.intValue ?? 0
  }
  
  var swellDir16Point: NSString?
  var _swellDir16Point: String {
    return swellDir16Point as String? ?? ""
  }
  
  var swellPeriod_secs: NSNumber?
  var _swellPeriod_secs: Double {
    return swellPeriod_secs?.doubleValue ?? 0
  }
  
  var waterTemp_C: NSNumber?
  var _waterTemp_C: Int {
    return waterTemp_C?.intValue ?? 0
  }
  
  var waterTemp_F: NSNumber?
  var _waterTemp_F: Int {
    return waterTemp_F?.intValue ?? 0
  }
}

class WeatherIconUrl: EVObject {
  var value: NSString?
  var _value: String {
    return value as String? ?? ""
  }
}

class WeatherDesc: EVObject {
  var value: NSString?
  var _value: String {
    return value as String? ?? ""
  }
}

class LangVi: EVObject {
  var value: NSString?
  var _value: String {
    return value as String? ?? ""
  }
}

