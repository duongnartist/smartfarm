//
//  MenuController.swift
//  SmartFarm
//
//  Created by Nguyễn Dương on 8/17/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit

class MenuController: UIViewController {

  @IBOutlet weak var avatarImageView: BorderCornerImageView!
  @IBOutlet weak var coverImageView: BorderCornerImageView!
  
  @IBOutlet weak var fullnameLabel: UILabel!
  @IBOutlet weak var roleLabel: UILabel!
  @IBOutlet weak var addressLabel: UILabel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    displayUserProfile()
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }

  func displayUserProfile() {
    if let full_name = UserObj.user.full_name {
      fullnameLabel.text = full_name as String
    } else {
      fullnameLabel.text = ""
    }
    
    if let role_name = UserObj.user.role_name {
      roleLabel.text = role_name as String
    } else {
      roleLabel.text = ""
    }
   
    if let address = UserObj.user.address {
      addressLabel.text = address as String
    } else {
      addressLabel.text = ""
    }
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    menuContainerViewController.toggleLeftSideMenuCompletion({
      
    })
  }
  
  
  
//  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//    if indexPath.section == 2 && indexPath.row == 0 {
//      view.window?.rootViewController = R.storyboard.main.loginController()
//    }
//  }
}
