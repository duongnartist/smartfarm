//
//  FeedController.swift
//  SmartFarm
//
//  Created by Nguyễn Dương on 8/27/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit

class FeedController: UIViewController {

  @IBOutlet weak var indexLabel: UILabel!
  @IBOutlet weak var timeLabel: UILabel!
  @IBOutlet weak var foodTypeLabel: UILabel!
  @IBOutlet weak var foodTypeButton: BorderCornerButton!


  var types = [String]()
  var selectedTypeIndex = 0

  var weights = [String]()
  var selectedWeightIndex = 0

  var selectedDate = Date()

  var lake = LakeObj()

  override func viewDidLoad() {
    super.viewDidLoad()
    for type in FoodTypeObj.foodTypeObjs {
      types.append(type._type_name)
    }
    self.foodTypeLabel.text = self.types[self.selectedTypeIndex]

    timeLabel.text = selectedDate.toString(format: "hh:mm")
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }

  @IBAction func backButtonTouchUpInside(_ sender: Any) {
    navigationController?.popViewController(animated: true)
  }

  @IBAction func doneButtonTouchUpInside(_ sender: Any) {
//    Request.setFoodToLake(lake_id: <#T##Int#>, time: <#T##Int#>, food_type_id: <#T##Int#>, hour: <#T##Int#>, minute: <#T##Int#>, food_val: <#T##Int#>, complete: <#T##(BaseRes) -> ()#>)
    showDialog(titleContent: "Thông báo", bodyContent: "Chúc mừng bạn đã cho tôm ăn xong!", leftButtonTitle: "Cancel", rightButtonTitle: "Đóng", onTouchedLeftButton: nil, onTouchedRightButton: {
      
    })
  }

  @IBAction func timeButtonTouchUpInside(_ sender: Any) {
    showTimePicker()
  }

  @IBAction func foodTypeButtonTouchUpInside(_ sender: Any) {
    showTypePicker()
  }

  func showTimePicker() {
    let picker = ActionSheetDatePicker(title: R.string.main.title_time_picker(), datePickerMode: .time, selectedDate: selectedDate, doneBlock: {
      _, value, _ in
      if let date: Date = value as? Date {
        self.selectedDate = date
        self.timeLabel.text = self.selectedDate.toString(format: "hh:mm")
      }
    }, cancel: {
      _ in
    }, origin: timeLabel)
    picker?.setDoneButton(UIBarButtonItem(title: "Chọn", style: .plain, target: nil, action: nil))
    picker?.setCancelButton(UIBarButtonItem(title: "Hủy", style: .plain, target: nil, action: nil))
    picker?.show()
  }

  func showTypePicker() {
    let picker = ActionSheetStringPicker(title: R.string.main.title_food_type_picker(), rows: types, initialSelection: selectedTypeIndex, doneBlock: {
      _, index, _ in
      if index >= 0 && index < self.types.count {
        self.selectedTypeIndex = index
        self.foodTypeLabel.text = self.types[self.selectedTypeIndex]
      }
    }, cancel: {
      _ in
    }, origin: foodTypeButton)
    picker?.setDoneButton(UIBarButtonItem(title: "Chọn", style: .plain, target: nil, action: nil))
    picker?.setCancelButton(UIBarButtonItem(title: "Hủy", style: .plain, target: nil, action: nil))
    picker?.show()
  }
}
