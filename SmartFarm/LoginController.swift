//
//  LoginViewController.swift
//  SmartFarm
//
//  Created by Nguyễn Dương on 8/17/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit
import EZAlertController
import EZSwiftExtensions
import CryptoSwift
import MFSideMenu

class LoginController: UIViewController {

  @IBOutlet weak var usernameTextField: UITextField!
  @IBOutlet weak var passwordTextField: UITextField!
  @IBOutlet weak var loginButton: BorderCornerButton!
  
  var isRequesting = false
  
  override func viewDidLoad() {
    super.viewDidLoad()
//    if UserObj.user.open() {
//      login(user: UserObj.user)
//    }
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }

  func prepare(complete: (_ user: UserObj, _ index: Int, _ value: String) -> ()) {
    let user = UserObj()
    user.username = usernameTextField.text! as NSString
    if !(user.username?.isUsername())! {
      complete(user, 0, R.string.main.invalid_username())
    }
    user.password = passwordTextField.text! as NSString
    if !(user.password?.isPassword())! {
      complete(user, 1, R.string.main.invalid_password())
    } else {
      user.password = (user.password! as String).md5() as NSString
    }
    complete(user, -1, "")
  }
  
  func login(user: UserObj) {
    show(requesting: &isRequesting)
    Request.login(user: user, complete: { res in
      self.hide(requesting: &self.isRequesting)
      if res.isSuccess {
//        self.displayManagerViewController()
      } else {
        EZAlertController.alert(R.string.main.title_failure(), message: res._msg, acceptMessage: R.string.main.button_ok(), acceptBlock: {
          
        })
      }
    })
  }
  
//  func displayManagerViewController() {
//    let container = MFSideMenuContainerViewController()
//    container.centerViewController = R.storyboard.main.managerTabBarController()!
//    container.leftMenuViewController = R.storyboard.main.menuController()!
//    view.window?.rootViewController = container
//  }
//  
  func displayStaffViewController() {
    let container = MFSideMenuContainerViewController()
    container.centerViewController = R.storyboard.main.staffTabController()!
    container.leftMenuViewController = R.storyboard.main.menuController()!
    view.window?.rootViewController = container
  }
  
  @IBAction func testManagerButtonTouchUpInside(_ sender: Any) {
//    displayManagerViewController()
  }
  
  @IBAction func testStaffButtonTouchUpInside(_ sender: Any) {
    displayStaffViewController()
  }
  
  
  @IBAction func loginButtonTouchUpInside(_ sender: Any) {
    guard isRequesting else {
      prepare(complete: { user, index, value in
        if index < 0 {
          self.usernameTextField.text = ""
          self.passwordTextField.text = ""
          login(user: user)
        } else {
          EZAlertController.alert(R.string.main.title_warning(), message: value, acceptMessage: R.string.main.button_ok(), acceptBlock: {
            if index == 0 {
              self.usernameTextField.becomeFirstResponder()
            }
            if index == 1 {
              self.passwordTextField.becomeFirstResponder()
            }
          })
        }
      })
      return
    }
  }
}

extension LoginController: UITextFieldDelegate {
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    if textField == usernameTextField {
      passwordTextField.becomeFirstResponder()
    }
    if textField == passwordTextField {
      passwordTextField.resignFirstResponder()
      loginButtonTouchUpInside(loginButton)
    }
    return true
  }
}
