//
//  PaymentController.swift
//  SmartFarm
//
//  Created by Nguyễn Dương on 8/19/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit

class PaymentController: UITableViewController {
  
  @IBOutlet weak var typeTitleLabel: UILabel!
  @IBOutlet weak var typeLabel: UILabel!
  var types = [String]()
  var selectedTypeIndex = 0
  
  override func viewDidLoad() {
    super.viewDidLoad()
    types = [R.string.main.payment_type_0(), R.string.main.payment_type_1()]
    typeLabel.text = types[selectedTypeIndex]
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    if indexPath.row == 5 {
      showPaymentPicker()
    }
  }
  
  func showPaymentPicker() {
    ActionSheetStringPicker(title: typeTitleLabel.text, rows: types, initialSelection: selectedTypeIndex, doneBlock: { picker, index, value in
      if index >= 0 && index < self.types.count {
        self.selectedTypeIndex = index
        self.typeLabel.text = self.types[self.selectedTypeIndex]
      }
    }, cancel: {
      _ in
    }, origin: typeLabel).show()
  }
}
