//
//  LakeCell.swift
//  SmartFarm
//
//  Created by Nguyễn Dương on 8/17/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit

class LakeCell: UICollectionViewCell {
    
  @IBOutlet weak var amountLabel: BorderCornerLabel!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var dateLabel: UILabel!
  @IBOutlet weak var statusLabel: BorderCornerLabel!
  
  func display(Lake: LakeObj) {
    if let acreage = Lake.acreage, let amount_brood = Lake.amount_brood {
      amountLabel.text = "\(amount_brood.formattedNumber)/\(acreage.formattedNumber)"
    } else {
      amountLabel.text = "0/0"
    }
    if let lake_name = Lake.lake_name {
      nameLabel.text = lake_name as String
    } else {
      nameLabel.text = ""
    }
    if let create_date = Lake.create_date {
      let date = Date(timeIntervalSince1970: TimeInterval(create_date))
      dateLabel.text = date.ddMMyyyy
    } else {
      dateLabel.text = ""
    }
  }
}
