//
//  DialogController.swift
//  SmartFarm
//
//  Created by Nguyễn Dương on 9/1/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit

class DialogController: UIViewController {
  
  @IBOutlet weak var leftButton: BorderCornerButton!
  @IBOutlet weak var rightButton: BorderCornerButton!
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var bodyLabel: UILabel!
  
  var leftButtonTitle = ""
  var rightButtonTitle = ""
  var titleContent = ""
  var bodyContent = ""
  
  var onTouchedLeftButton: (() -> ())!
  var onTouchedRightButton: (() -> ())!

  override func viewDidLoad() {
    super.viewDidLoad()

    leftButton.setTitle(leftButtonTitle, for: .normal)
    rightButton.setTitle(rightButtonTitle, for: .normal)
    
    titleLabel.text = titleContent
    bodyLabel.text = bodyContent
    
    leftButton.isHidden = onTouchedLeftButton == nil
    rightButton.isHidden = onTouchedRightButton == nil
    
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }

  @IBAction func leftButtonTouchUpInside(_ sender: Any) {
    dismiss(animated: true, completion: {
      if self.onTouchedLeftButton != nil {
        self.onTouchedLeftButton()
      }
    })
  }
  
  @IBAction func rightButtonTouchUpInside(_ sender: Any) {
    dismiss(animated: true, completion: {
      if self.onTouchedRightButton != nil {
        self.onTouchedRightButton()
      }
    })
  }
}

extension UIViewController {
  
  func showDialog(titleContent: String, bodyContent: String, leftButtonTitle: String, rightButtonTitle: String, onTouchedLeftButton: (() -> ())!, onTouchedRightButton: (() -> ())!) {
    let dialogController = R.storyboard.dialog.dialogController()
    dialogController?.modalTransitionStyle = .crossDissolve
    dialogController?.modalPresentationStyle = .overFullScreen
    
    dialogController?.titleContent = titleContent
    dialogController?.bodyContent = bodyContent
    
    dialogController?.leftButtonTitle = leftButtonTitle
    dialogController?.rightButtonTitle = rightButtonTitle
    
    dialogController?.onTouchedLeftButton = onTouchedLeftButton
    dialogController?.onTouchedRightButton = onTouchedRightButton
    
    present(dialogController!, animated: true, completion: nil)
  }
  
}
