//
//  WeatherController.swift
//  SmartFarm
//
//  Created by Nguyễn Dương on 9/1/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit

class WeatherController: UIViewController {
  
  @IBOutlet weak var collectionView: UICollectionView!
  @IBOutlet weak var iconImageView: UIImageView!
  @IBOutlet weak var weatherLabel: UILabel!
  @IBOutlet weak var tempLabel: UILabel!
  @IBOutlet weak var locationLabel: UILabel!
  @IBOutlet weak var tempRangeLabel: UILabel!
  
  var currentWeather = WeatherRes()

  override func viewDidLoad() {
    super.viewDidLoad()

    Request.getCurrentWeather(latitude: UserObj.latitude, longitude: UserObj.longitude, handler: { weather in
      print(weather)
      self.currentWeather = weather
      self.displayCurrentWeather()
      self.collectionView.reloadData()
    })
  }
  
  func displayCurrentWeather() {
//    locationLabel.text = currentWeather._data.
//    tempLabel.text = String(format: "%.0f °C", currentWeather._main._temp_c)
//    tempRangeLabel.text = String(format: "%.0f - %.0f °C", currentWeather._main._temp_min_c, currentWeather._main._temp_max_c)
//    weatherLabel.text = currentWeather._data.weather.first?.hourly.first._
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }

  @IBAction func closeButtonTouchUpInside(_ sender: UIButton) {
    dismiss(animated: true, completion: nil)
  }
}

extension WeatherController: UICollectionViewDelegate, UICollectionViewDataSource {
  
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return currentWeather._data.weather.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.weatherCell.identifier, for: indexPath) as! WeatherCell
    if let weatherObj = self.currentWeather._data.weather.get(at: indexPath.row) {
      cell.display(weatherObj: weatherObj, isC: true)
    }
    return cell
  }
  
}
