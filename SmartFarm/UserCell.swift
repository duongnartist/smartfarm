//
//  UserCell.swift
//  SmartFarm
//
//  Created by Nguyễn Dương on 8/19/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit

class UserCell: UITableViewCell {
  
  @IBOutlet weak var avatarImageView: BorderCornerImageView!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var emailLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

  func display(user: UserObj) {
    nameLabel.text = user.getFullName()
    emailLabel.text = user.getEmail()
  }

}
