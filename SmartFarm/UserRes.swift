//
//  UserRes.swift
//  SmartFarm
//
//  Created by Nguyễn Dương on 8/17/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import Foundation
import EVReflection

class UserRes: BaseRes {
  var data: UserObj?
}

class UserObj: EVObject {
  
  static var user = UserObj()
  static var latitude = 0.0
  static var longitude = 0.0
  
  static func openLocation() -> Bool {
    var result = false
    if let latitude = UserDefaults.standard.object(forKey: "latitude") {
      UserObj.latitude = latitude as! Double
      result = true
    } else {
      
    }
    if let longitude = UserDefaults.standard.object(forKey: "longitude") {
      UserObj.longitude = longitude as! Double
      result = true
    } else {
      
    }
    print("Open Location \(latitude) - \(longitude)")
    return result
  }
  
  static func saveLocation() {
    UserDefaults.standard.set(latitude, forKey: "latitude")
    UserDefaults.standard.set(longitude, forKey: "longitude")
    print("Save Location \(latitude) - \(longitude)")
  }
  
  var password: NSString? = ""
  var user_id: NSString? = ""
  var username: NSString? = ""
  var full_name: NSString? = ""
  
  func getFullName() -> String {
    if let value = full_name {
      return value as String
    }
    return ""
  }
  
  var address: NSString? = ""
  
  func getAddress() -> String {
    if let value = address {
      return value as String
    }
    return ""
  }
  
  var phone_number: NSString? = ""
  
  func getPhoneNumber() -> String {
    if let value = phone_number {
      return value as String
    }
    return ""
  }
  
  var email: NSString? = ""
  
  func getEmail() -> String {
    if let value = email {
      return value as String
    }
    return ""
  }
  
  var role: NSString? = ""
  
  func getRole() -> String {
    if let value = role {
      return value as String
    }
    return ""
  }
  
  var role_name: NSString? = ""
  
  func getRoleName() -> String {
    if let value = role_name {
      return value as String
    }
    return ""
  }
  
  var status: NSNumber? = 0
  
  func getStatus() -> Int {
    if let value = status {
      return value as! Int
    }
    return 0
  }
  
  var create_date: NSNumber? = 0
  
  func getCreateDate() -> Int {
    if let value = create_date {
      return value as! Int
    }
    return 0
  }
  
  func getCreateDateString() -> String {
    let timestamp = getCreateDate()
    return Date(timeIntervalSince1970: TimeInterval(timestamp)).toString(format: "dd-MM-yyyy")
  }
  
  func open() -> Bool {
    if let username = UserDefaults.standard.object(forKey: "username") {
      self.username = username as? NSString
    }
    if let password = UserDefaults.standard.object(forKey: "password") {
      self.password = password as? NSString
    }
    if let username = self.username, let password = self.password {
      if username.length > 0 && password.length > 0 {
        return true
      }
    }
    return false
  }
  
  func save() {
    UserDefaults.standard.set(username, forKey: "username")
    UserDefaults.standard.set(password, forKey: "password")
  }
}
