//
//  StaffBottomController.swift
//  SmartFarm
//
//  Created by Nguyễn Dương on 9/1/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit

class StaffBottomController: UIViewController {

  @IBOutlet weak var imageView0: UIImageView!
  @IBOutlet weak var imageView1: UIImageView!
  @IBOutlet weak var imageView2: UIImageView!
  @IBOutlet weak var imageView3: UIImageView!

  @IBOutlet weak var label0: UILabel!
  @IBOutlet weak var label1: UILabel!
  @IBOutlet weak var label2: UILabel!
  @IBOutlet weak var label3: UILabel!
  
  var unSelectedColor = UIColor(hexString: "#6D797A")
  var selectedColor = UIColor(hexString: "#3BC651")
  
  var imageViews = [UIImageView]()
  var labels = [UILabel]()
  
  var selectedImages = [UIImage]()
  var unSelectedImages = [UIImage]()
  
  var onSelectedTab: ((_ index: Int) -> ())!
  var selectedTabIndex = -1

  override func viewDidLoad() {
    super.viewDidLoad()
    imageViews = [imageView0, imageView1, imageView2, imageView3]
    labels = [label0, label1, label2, label3]
    selectedImages = [R.image.ic_tab_food_on()!, R.image.ic_tab_pill_on()!, R.image.ic_tab_grid_on()!, R.image.ic_tab_evn_on()!]
    unSelectedImages = [R.image.ic_tab_food_off()!, R.image.ic_tab_pill_off()!, R.image.ic_tab_grid_off()!, R.image.ic_tab_evn_off()!]
    
    selectTab(index: 0)
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
  
  func selectTab(index: Int) {
    if selectedTabIndex != index {
      self.selectedTabIndex = index
      for i in 0 ..< labels.count {
        labels[i].textColor = i == index ? selectedColor : unSelectedColor
        imageViews[i].image = i == index ? selectedImages[i] : unSelectedImages[i]
      }
      onSelectedTab(index)
    }
  }

  @IBAction func button0TouchUpInside(_ sender: Any) {
    selectTab(index: 0)
  }
  
  @IBAction func button1TouchUpInside(_ sender: Any) {
    selectTab(index: 1)
  }
  
  @IBAction func button2TouchUpInside(_ sender: Any) {
    selectTab(index: 2)
  }
  
  @IBAction func button3TouchUpInside(_ sender: Any) {
    selectTab(index: 3)
  }
}
