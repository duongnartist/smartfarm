//
//  Extensions.swift
//  Hozina
//
//  Created by Nguyễn Dương on 8/8/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import Foundation
import UIKit
import ESPullToRefresh
import Foundation
import UIKit
import SVProgressHUD
import EZSwiftExtensions

extension UIView {
  
  func make(cornerRadius: CGFloat) {
    layer.masksToBounds = true
    layer.cornerRadius = cornerRadius
  }
  
  func make(borderWidth: CGFloat, borderColor: CGColor) {
    layer.masksToBounds = true
    layer.borderWidth = borderWidth
    layer.borderColor = borderColor
  }
  
  func shadow() {
    self.clipsToBounds = false
    self.layer.masksToBounds = false
    self.layer.shadowOpacity = 0.25
    self.layer.shadowRadius = 3
    self.layer.shadowOffset = CGSize(width: 0, height: 0)
    self.layer.shadowColor = UIColor.black.cgColor
    self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: 3).cgPath
  }
  
}

extension UITableView {
  
  func stopLoading() {
    es_stopLoadingMore()
    es_stopPullToRefresh()
  }
  
}

extension UICollectionView {
  
  func stopLoading() {
    es_stopLoadingMore()
    es_stopPullToRefresh()
  }
  
}

extension Formatter {
  static let withSeparator: NumberFormatter = {
    let formatter = NumberFormatter()
    formatter.groupingSeparator = "."
    formatter.numberStyle = .decimal
    return formatter
  }()
}

extension Int {
  var formattedWithSeparator: String {
    return Formatter.withSeparator.string(for: self) ?? ""
  }
}

extension NSNumber {
  
  var price: String {
    if let intValue: Int = self as? Int {
      return "\(intValue.formattedWithSeparator) đ"
    }
    return " đ"
  }
  
  var formattedNumber: String {
    if let intValue: Int = self as? Int {
      return intValue.formattedWithSeparator
    }
    return ""
  }
}

extension Date {
  var ddMMyyyy: String {
    return self.toString(format: "dd-MM-yyyy")
  }
}

extension NSString {
  func isUsername() -> Bool {
    return self.length >= 6
  }
  
  func isPassword() -> Bool {
    return self.length >= 6
  }
}

extension String {
  
  var isPassword: Bool {
    return self.length >= 8
  }
  
  var isPhone: Bool {
    return self.length >= 10
  }
  
  var length: Int {
    return characters.count
  }
  
  var ddMMyyy: String {
    if let date = Date(fromString: self, format: "yyyy-MM-dd hh:mm:ss") {
      return date.ddMMyyyy
    }
    return ""
  }
}

extension NSMutableAttributedString{
  func setColorForText(_ textToFind: String, with color: UIColor) {
    let range = self.mutableString.range(of: textToFind, options: .caseInsensitive)
    if range.location != NSNotFound {
      addAttribute(NSForegroundColorAttributeName, value: color, range: range)
    }
  }
}

extension UIViewController {
  
  func show(requesting: inout Bool) {
    requesting = true
    view.isUserInteractionEnabled = !requesting
    SVProgressHUD.show()
  }
  
  func hide(requesting: inout Bool) {
    requesting = false
    view.isUserInteractionEnabled = !requesting
    SVProgressHUD.dismiss()
  }
  
  //  func getIndicator(title: String, body: String) -> IndicatorVC {
  //    let vc = R.storyboard.main.indicatorVC()
  //    vc?.modalPresentationStyle = .overFullScreen
  //    vc?.modalTransitionStyle = .crossDissolve
  //    vc?.titleString = title
  //    vc?.bodyString = body
  //    return vc!
  //  }
  //
  //  func show(indicator: IndicatorVC) {
  //    present(indicator, animated: true, completion: nil)
  //  }
  //
  //  func hide(indicator: IndicatorVC) {
  //    indicator.dismiss(animated: true, completion: nil)
  //  }
}

extension UITextField {
  @IBInspectable var placeHolderTextColor: UIColor? {
    set {
      let placeholderText = self.placeholder != nil ? self.placeholder! : ""
      attributedPlaceholder = NSAttributedString(string: placeholderText, attributes: [NSForegroundColorAttributeName: newValue!])
    }
    get {
      return self.placeHolderTextColor
    }
  }
}
