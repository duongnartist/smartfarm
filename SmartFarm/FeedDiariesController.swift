//
//  FeedDiariesController.swift
//  SmartFarm
//
//  Created by Nguyễn Dương on 8/27/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit
import MFSideMenu

class FeedDiariesController: UIViewController {

  @IBOutlet weak var todayLabel: UILabel!
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var collectionView: UICollectionView!
  var lakes = [LakeObj]()
  var feeds = [FeedObj]()

  var isRequesting = false
  var selectedLakeIndex = 0

  override func viewDidLoad() {
    super.viewDidLoad()
    self.reloadFoodType()
    self.reloadLakeData()

    for i in 0 ... 20 {
      let feed = FeedObj()
      feed.id = i as NSNumber
      feed.time = Date().addingTimeInterval(TimeInterval(i * 3600)).toString(format: "hh:mm") as NSString
      feed.weight = 1 + i * 2 as NSNumber
      feed.number = "Number \(i)" as NSString
      feeds.append(feed)
    }
  }
  
  func reloadFoodType() {
    Request.getFoodType(complete: {
      res in
      print(res)
    })
  }

  func reloadLakeData() {
    guard isRequesting else {
      if self.lakes.isEmpty {
        show(requesting: &self.isRequesting)
        Request.getAllLake(complete: {
          res in
//          print(res)
          self.hide(requesting: &self.isRequesting)
          if res.isSuccess {
            self.lakes = res.data
            self.collectionView.reloadData()
          }
        })
      } else {
        self.lakes = LakeObj.lakeObjs
        self.collectionView.reloadData()
      }
      return
    }
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(true)
    let dateString = Date().toString(format: "dd - MM - yyyy")
    todayLabel.text = R.string.main.title_lake_day(dateString)
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()

  }

  @IBAction func menuButtonTouchUpInside(_ sender: Any) {
    NotificationCenter.default.post(name: NSNotification.Name("ShowLeftMenu"), object: nil)
  }
}

extension FeedDiariesController: UITableViewDelegate, UITableViewDataSource {

  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.feeds.count
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.feedCell.identifier, for: indexPath) as! FeedCell
    if let feed = self.feeds.get(at: indexPath.row) {
      cell.display(feed: feed)
    }
    return cell
  }

}

extension FeedDiariesController: UICollectionViewDelegate, UICollectionViewDataSource {

  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }

  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return self.lakes.count
  }

  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.miniLakeCell.identifier, for: indexPath) as! MiniLakeCell
    if let lake = lakes.get(at: indexPath.row) {
      cell.display(lake: lake)
    }
    return cell
//    return UICollectionViewCell()
  }

  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    for i in 0 ..< self.lakes.count {
      self.lakes[i].isSelected = i == indexPath.row
    }
    self.collectionView.reloadData()
  }
}
