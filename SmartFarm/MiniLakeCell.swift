//
//  MiniLakeCell.swift
//  SmartFarm
//
//  Created by Nguyễn Dương on 8/27/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit

class MiniLakeCell: UICollectionViewCell {
    
  @IBOutlet weak var backView: BorderCornerView!
  @IBOutlet weak var nameLabel: UILabel!
  
  var selectedColor = UIColor(hexString: "#3182D9")
  var unSelectedColor = UIColor(hexString: "#ffffff")
  
  func display(lake: LakeObj) {
    nameLabel.text = lake._lake_name
    nameLabel.textColor = lake.isSelected ? unSelectedColor : selectedColor
    backView.backgroundColor = lake.isSelected ? selectedColor : unSelectedColor
  }
}
