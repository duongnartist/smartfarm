//
//  BaseRes.swift
//  SmartFarm
//
//  Created by Nguyễn Dương on 8/17/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import Foundation
import EVReflection

class BaseRes: EVObject {
  
  var status: NSNumber? = 0
  var time: NSNumber? = 0
  var msg: NSString? = ""
  var token: NSString? = ""
  
  var isSuccess: Bool {
    return _status > 0
  }
  
  var _status: Int {
    return status?.intValue ?? 0
  }
  
  var _msg: String {
    return msg as String? ?? ""
  }
  
  var _token: String {
    return token as String? ?? ""
  }
  
  var _time: Int {
    return time?.intValue ?? 0
  }
}
