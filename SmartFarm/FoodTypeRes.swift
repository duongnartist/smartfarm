//
//  FoodTypeRes.swift
//  SmartFarm
//
//  Created by Nguyễn Dương on 8/26/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import Foundation
import EVReflection

class FoodTypeRes: BaseRes {
  var data: [FoodTypeObj] = []
}

class FoodTypeObj: EVObject {
  
  static var foodTypeObjs = [FoodTypeObj]()
  
  var type_id: NSString? = ""
  
  var _type_id: String {
    return type_id as String? ?? ""
  }
  
  var type_name: NSString? = ""
  
  var _type_name: String {
    return type_name as String? ?? ""
  }
  
  var order: NSNumber? = 0

  var _order: Int {
    return order?.intValue ?? 0
  }
  
  var value1: NSNumber? = 0
  
  var _value1: Double {
    return value1?.doubleValue ?? 0.0
  }
  
  var value2: NSNumber? = 0
  
  var _value2: Double {
    return value2?.doubleValue ?? 0.0
  }
  
}
