//
//  UsersController.swift
//  SmartFarm
//
//  Created by Nguyễn Dương on 8/19/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import UIKit

class UsersController: UITableViewController {

  var userData = [UserObj]()

  override func viewDidLoad() {
    super.viewDidLoad()
    for i in 0 ... 10 {
      let user = UserObj()
      user.full_name = "Name \(i)" as NSString
      user.email = "Email\(i)@gmail.com" as NSString
      userData.append(user)
    }
  }
  
  override func numberOfSections(in tableView: UITableView) -> Int {
    return 3
  }

  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return userData.count
  }

  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//    let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.userCell.identifier, for: indexPath) as! UserCell
//    cell.display(user: userData[indexPath.row])
//    return cell
    return UITableViewCell()
  }

  @IBAction func closeAction(_ sender: Any) {
    navigationController?.dismiss(animated: true, completion: nil)
  }
}
