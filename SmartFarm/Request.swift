//
//  Request.swift
//  SmartFarm
//
//  Created by Nguyễn Dương on 8/17/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import Foundation
import Alamofire

class Request {
  struct api {
    static let errorFormat = "{\"code\": 400, \"message\": \"%@\", \"result\": [] }"
    static let baseUrl = "http://qltrangtrai.sunjsc.vn"
    static let uploadUrl = "\(baseUrl)/upload/"
    static let weatherApiKey = "1e84c389a73d4daea7f232252170109"
  }
  
  static func getCurrentWeather(latitude: Double, longitude: Double, handler: @escaping (_ res: WeatherRes) -> ()) {
    let url = "http://api.worldweatheronline.com/premium/v1/marine.ashx"
    let params: [String: Any] = [
      "q": "\(latitude),\(longitude)",
      "fx": "yes",
      "format": "json",
      "tp": 24,
      "tide": "yes",
      "lang": "vi",
      "key": api.weatherApiKey
    ]
    Alamofire.request(url, method: .get, parameters: params).responseJSON(completionHandler: { res in
      print(res.request?.url?.absoluteString ?? "")
      if res.result.isSuccess {
        if let data = res.data {
          let res = WeatherRes(data: data)
          handler(res)
        } else {
          
        }
      } else {
//        let errorString = String(format: api.errorFormat, R.string.main.request_error())
//        response(errorString.data(using: .utf8)!)
      }
    })
  }
  
  // 1. Login
  static func login(user: UserObj, complete: @escaping (_ res: UserRes) -> ()) {
    let uri = "/rest/v1/user/login"
    let params: [String: Any] = [
      "username": user.username!,
      "password": user.password!
    ]
    post(uri: uri, params: params, response: { data in
      let res = UserRes(data: data)
      if res.isSuccess {
        if let userObj = res.data {
          UserObj.user = userObj
          UserObj.user.password = user.password
          UserObj.user.save()
        }
      }
      print("\t- uri: \(uri)")
      print("\t- params: \(params)")
      print("\t- res: \(res)")
      complete(res)
    })
  }
  // 2
  static func getAllLake(complete: @escaping (_ res: LakeRes) -> ()) {
    let uri = "/rest/v1/lake/getAll"
    get(uri: uri, params: [:], response: { data in
      let res = LakeRes(data: data)
      if res.isSuccess {
        LakeObj.lakeObjs = res.data
      }
      complete(res)
    })
  }
  
  // 3. Lấy thông tin loại thức ăn
  static func getFoodType(complete: @escaping (_ res: FoodTypeRes) -> ()) {
    let uri = "/rest/v1/lake/getFoodType"
    get(uri: uri, params: [:], response: { data in
      let res = FoodTypeRes(data: data)
      if res.isSuccess {
        FoodTypeObj.foodTypeObjs = res.data
      }
      complete(res)
    })
  }
  
  // 4. Nhập thức ăn
  static func setFoodToLake(lake_id: Int, time: Int, food_type_id: Int, hour: Int, minute: Int, food_val: Int, complete: @escaping (_ res: BaseRes) -> ()) {
    let uri = "/rest/v1/lake/setFoodToLake"
    let params: [String: Any] = [
      "lake_id": lake_id,
      "time": time,
      "food_type_id": food_type_id,
      "hour": hour,
      "minute": minute,
      "food_val": food_val
    ]
    post(uri: uri, params: params, response: { data in
      print("\t- uri: \(uri)")
      print("\t- params: \(params)")
      let res = BaseRes(data: data)
      print("\t- res: \(res)")
      if res.isSuccess {
        
      }
      complete(res)
    })
  }
  
  static func get(
    uri: String,
    params: [String: Any],
    response: @escaping (_ data: Data) -> ()
  ) {
    let url = api.baseUrl + uri
    let parameters = create(params: params, request: url)
    Alamofire.request(url, method: .get, parameters: parameters).responseJSON(completionHandler: { res in
      print(res.request?.url?.absoluteString ?? "")
      if res.result.isSuccess {
        response(res.data!)
      } else {
        let errorString = String(format: api.errorFormat, R.string.main.request_error())
        response(errorString.data(using: .utf8)!)
      }
    })
  }
  
  static func post(
    uri: String,
    params: [String: Any],
    response: @escaping (_ data: Data) -> ()
  ) {
    let url = api.baseUrl + uri
    let parameters = create(params: params, request: uri)
    Alamofire.request(url, method: .post, parameters: parameters).responseJSON(completionHandler: { res in
      print(res.request?.url?.absoluteString ?? "")
      if res.result.isSuccess {
        response(res.data!)
      } else {
        let errorString = String(format: api.errorFormat, R.string.main.request_error())
        response(errorString.data(using: .utf8)!)
      }
    })
  }
  
  static func create(
    params: [String: Any]?,
    request: String
  ) -> [String: Any] {
    var parameters = [String: Any]()
    if let params = params {
      parameters = params
    }
    return parameters
  }
}
