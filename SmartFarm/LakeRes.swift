//
//  LakeRes.swift
//  SmartFarm
//
//  Created by Nguyễn Dương on 8/17/17.
//  Copyright © 2017 Nguyễn Dương. All rights reserved.
//

import Foundation
import EVReflection

class LakeRes: BaseRes {
  var data: [LakeObj] = []
}

class LakeObj: EVObject {
  
  static var lakeObjs = [LakeObj]()
  
  var isSelected = false
  
  var lake_id: NSString? = ""
  var _lake_id: String {
    return lake_id as String? ?? ""
  }
  
  var lake_name: NSString? = ""
  var _lake_name: String {
    return lake_name as String? ?? ""
  }
  
  var amount_brood: NSNumber? = 0
  var _amount_brood: Int {
    return amount_brood?.intValue ?? 0
  }
  
  var acreage: NSNumber? = 0
  var _acreage: Int {
    return acreage?.intValue ?? 0
  }
  
  var season: NSNumber? = 0
  var _season: Int {
    return season?.intValue ?? 0
  }
  
  var water_level: NSNumber? = 0
  var _water_level: Int {
    return water_level?.intValue ?? 0
  }
  
  var start_date: NSString? = ""
  var _start_date: String {
    return start_date as String? ?? ""
  }
  
  var seed_source: NSString? = ""
  var _seed_source: String {
    return seed_source as String? ?? ""
  }
  
  var status: NSNumber? = 0
  var _status: Int {
    return status?.intValue ?? 0
  }
  
  var create_date: NSNumber? = 0
  var _create_date: Int {
    return create_date?.intValue ?? 0
  }
  
  var drug_start_date: NSString? = ""
  var _drug_start_date: String {
    return drug_start_date as String? ?? ""
  }
  
  var note: NSString? = ""
  var _note: String {
    return note as String? ?? ""
  }
  
  var amount_per_kg: NSNumber? = 0
  var _amount_per_kg: Int {
    return amount_per_kg?.intValue ?? 0
  }
  
}
